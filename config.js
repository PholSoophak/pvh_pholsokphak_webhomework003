tailwind.config = {
  theme: {
    extend: {
      colors: {
        startColor: '#0081B4',
        startColor1: '#93BFCF',
        stopColor: '#0095ff99',
        stopColor1: '#00D7FF',

        stop:'#DD5353',
        play:'#0002A1',
        clear:'#FFE15D'
      },

      backgroundImage: {
        backgroundCover: "url('/img/bg1.jpg)",
      },

      font:{
        myFont: ["font-family: 'Freehand', cursive;"]
      }
    }
  }
}